package com.example.myapplication.data.repository

import android.content.SharedPreferences
import com.example.myapplication.domain.repository.SkipOnboarding

class SkipOnboardingImpl(private val sharedPreferences: SharedPreferences): SkipOnboarding {
    override fun getData(): Int {
        return sharedPreferences.getInt("skip", 0)
    }

    override fun writeData(data: Int) {
        sharedPreferences.edit().putInt("skip", data).apply()
    }
}