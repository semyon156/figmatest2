package com.example.myapplication.presentation.screens

import android.content.Context
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.BasicTextField
import androidx.compose.foundation.text.ClickableText
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.buildAnnotatedString
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.PasswordVisualTransformation
import androidx.compose.ui.text.input.VisualTransformation
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavController
import com.example.myapplication.R
import com.example.myapplication.presentation.navigation.Screen
import com.example.myapplication.presentation.ui.theme.Text_Black_1
import com.example.myapplication.presentation.ui.theme.Text_Gray_1
import com.example.myapplication.presentation.ui.theme.Text_Gray_2
import com.example.myapplication.presentation.ui.theme.Text_Yellow_1
import com.example.myapplication.presentation.ui.theme.roboto
import com.example.myapplication.presentation.viewmodels.SignupViewModel
import com.example.myapplication.presentation.viewmodels.SignupViewModelFactory
import io.github.jan.supabase.SupabaseClient

@Composable
fun BoxField(
    textValue: String,
    onTextChange: (String) -> Unit,
    hintText: String,
    onError: Boolean,
    isPassword: Boolean,
    headText: String
) {
    var clickEyePass by rememberSaveable { mutableStateOf(isPassword) }

    Column(modifier = Modifier.fillMaxWidth()) {
        Text(
            text = headText,
            style = TextStyle(
                fontFamily = roboto,
                fontSize = 14.sp,
                fontWeight = FontWeight.Medium,
                color = Text_Gray_1
            )
        )
        Spacer(modifier = Modifier.size(6.dp))
        Box(
            modifier = Modifier
                .height(44.dp)
                .fillMaxWidth()
                .background(
                    shape = RoundedCornerShape(4.dp),
                    color = MaterialTheme.colorScheme.background
                )
                .border(
                    1.dp,
                    color = if (onError) MaterialTheme.colorScheme.error else Text_Gray_1,
                    shape = RoundedCornerShape(4.dp)
                ),
            contentAlignment = Alignment.CenterStart
        ) {
            BasicTextField(
                value = textValue, onValueChange = { onTextChange(it) },
                textStyle = TextStyle(
                    fontFamily = roboto,
                    fontSize = 14.sp,
                    fontWeight = FontWeight.Medium,
                    color = if (onError) MaterialTheme.colorScheme.error else Text_Black_1
                ),
                modifier = Modifier
                    .padding(start = 10.dp)
                    .fillMaxWidth()
                    .padding(end = 30.dp),
                visualTransformation = if (!clickEyePass) VisualTransformation.None
                else PasswordVisualTransformation(mask = '*'),
                singleLine = true
            )
            if (textValue.isEmpty()) {
                Text(
                    text = hintText,
                    style = TextStyle(
                        fontFamily = roboto,
                        fontSize = 14.sp,
                        fontWeight = FontWeight.Medium,
                        color = Text_Gray_2
                    ),
                    modifier = Modifier.padding(start = 10.dp)
                )
            }
            if (isPassword) {
                Box(
                    modifier = Modifier
                        .fillMaxSize()
                        .padding(end = 10.dp), contentAlignment = Alignment.CenterEnd
                ) {
                    Icon(
                        painterResource(id = R.drawable.eye_pass),
                        contentDescription = "eye_pass",
                        modifier = Modifier.clickable { clickEyePass = !clickEyePass }
                    )
                }
            }
        }
    }
}

@Composable
fun SignUpScreen(
    context: Context,
    supabase: SupabaseClient,
    vm: SignupViewModel = viewModel(factory = SignupViewModelFactory(context = context, supabase = supabase)),
    navController: NavController
) {
    val textName = vm.signUpName.collectAsState().value
    val textPhone = vm.signUpPhone.collectAsState().value
    val textEmail = vm.signUpEmail.collectAsState().value
    val textPass = vm.signUpPass.collectAsState().value
    val textPass2 = vm.signUpPass2.collectAsState().value
    val checkBox = vm.checkBox.collectAsState().value
    val errorEmail = vm.errorEmail.collectAsState().value
    val errorPass = vm.errorPass.collectAsState().value

    Column(
        modifier = Modifier
            .fillMaxSize()
            .background(MaterialTheme.colorScheme.background)
            .padding(start = 20.dp, end = 20.dp)
            .verticalScroll(rememberScrollState()),
    ) {
        Spacer(modifier = Modifier.size(30.dp))
        Text(
            text = "Create an account",
            style = TextStyle(
                fontFamily = roboto,
                fontSize = 24.sp,
                fontWeight = FontWeight.Medium,
                color = Text_Black_1
            )
        )
        Spacer(modifier = Modifier.size(6.dp))
        Text(
            text = "Complete the sign up process to get started",
            style = TextStyle(
                fontFamily = roboto,
                fontSize = 14.sp,
                fontWeight = FontWeight.Medium,
                color = Text_Gray_1
            )
        )
        Spacer(modifier = Modifier.size(20.dp))
        BoxField(
            textValue = textName,
            onTextChange = { vm.updateSignUpName(it) },
            hintText = "Ivanov Ivan",
            onError = false,
            isPassword = false,
            headText = "Full name"
        )
        Spacer(modifier = Modifier.size(20.dp))
        BoxField(
            textValue = textPhone,
            onTextChange = { vm.updateSignUpPhone(it) },
            hintText = "+7(999)999-99-99",
            onError = false,
            isPassword = false,
            headText = "Phone Number"
        )
        Spacer(modifier = Modifier.size(20.dp))
        BoxField(
            textValue = textEmail,
            onTextChange = { vm.updateSignUpEmail(it) },
            hintText = "***********@mail.com",
            onError = errorEmail,
            isPassword = false,
            headText = "Email Address"
        )
        Spacer(modifier = Modifier.size(20.dp))
        BoxField(
            textValue = textPass,
            onTextChange = { vm.updateSignUpPass(it) },
            hintText = "**********",
            onError = errorPass,
            isPassword = true,
            headText = "Password"
        )
        Spacer(modifier = Modifier.size(20.dp))
        BoxField(
            textValue = textPass2,
            onTextChange = { vm.updateSignUpPass2(it) },
            hintText = "**********",
            onError = errorPass,
            isPassword = true,
            headText = "Confirm Password"
        )
        Spacer(modifier = Modifier.size(30.dp))
        Row(
            modifier = Modifier
        ) {
            Icon(
                painterResource(
                    if (checkBox) R.drawable.check_box_true
                    else R.drawable.check_box_false
                ),
                contentDescription = "check_box",
                tint = Color.Unspecified,
                modifier = Modifier
                    .padding(top = 1.dp)
                    .clickable { vm.updateCheckBox(!checkBox) }
            )
            Spacer(modifier = Modifier.size(20.dp))
            val annotatedString = buildAnnotatedString {
                val str1 = "By ticking this box, you agree to our "
                val str2 = "Terms and conditions and private policy"
                append(str1)
                addStyle(
                    style = SpanStyle(
                        fontFamily = roboto,
                        fontSize = 12.sp,
                        fontWeight = FontWeight.Normal,
                        color = Text_Gray_1,
                    ), start = 0, end = 38
                )
                append(str2)
                addStyle(
                    style = SpanStyle(
                        fontFamily = roboto,
                        fontSize = 12.sp,
                        fontWeight = FontWeight.Normal,
                        color = Text_Yellow_1,
                    ), start = 38, end = 77
                )
                addStringAnnotation("click", annotation = "123", start = 38, end = 77)
            }
            ClickableText(
                text = annotatedString,
                onClick = {
                    annotatedString.getStringAnnotations(tag = "click", it, it).firstOrNull()?.let {
                        vm.goToast("Нажатие на политику")
                    }
                },
                style = TextStyle(
                    textAlign = TextAlign.Center
                ),
                modifier = Modifier.padding(end = 40.dp)
            )
        }
        Spacer(modifier = Modifier.size(60.dp))
        Button(
            onClick = {
                if (textName != "" &&
                    textEmail != "" &&
                    textPhone != "" &&
                    textPass != "" &&
                    textPass2 != "" &&
                    checkBox
                ) {
                    vm.checkEmail()
                    vm.checkPass()
                    if (!vm.checkEmail() && !vm.checkPass()) {
                        /*navController.navigate(Screen.Login.route) {
                            popUpTo(0)
                        }*/
                        vm.signupUser()
                    }
                }
            },
            modifier = Modifier
                .height(46.dp)
                .fillMaxWidth(),
            shape = RoundedCornerShape(4.dp),
            colors = ButtonDefaults.buttonColors(
                containerColor = if (
                    textName != "" &&
                    textEmail != "" &&
                    textPhone != "" &&
                    textPass != "" &&
                    textPass2 != "" &&
                    checkBox
                )
                    MaterialTheme.colorScheme.primary else Text_Gray_1
            )
        ) {
            Text(
                text = "Sign Up",
                style = TextStyle(
                    fontFamily = roboto,
                    fontSize = 16.sp,
                    fontWeight = FontWeight.Bold
                )
            )
        }
        Spacer(modifier = Modifier.size(20.dp))
        Column(
            horizontalAlignment = Alignment.CenterHorizontally,
            modifier = Modifier.fillMaxWidth()
        ) {
            Row {
                Text(
                    text = "Already have an account?",
                    fontFamily = roboto,
                    fontSize = 14.sp,
                    fontWeight = FontWeight.Normal,
                    color = Text_Gray_1,
                )
                Text(
                    text = "Sign in",
                    fontFamily = roboto,
                    fontSize = 14.sp,
                    fontWeight = FontWeight.Medium,
                    color = MaterialTheme.colorScheme.primary,
                    modifier = Modifier.clickable {
                        navController.navigate(Screen.Login.route) {
                            popUpTo(0)
                        }
                    }
                )
            }
            Spacer(modifier = Modifier.size(20.dp))
            Text(
                text = "or sign in using",
                fontFamily = roboto,
                fontSize = 14.sp,
                fontWeight = FontWeight.Normal,
                color = Text_Gray_1
            )
            Spacer(modifier = Modifier.size(6.dp))
            Icon(
                painterResource(id = R.drawable.google_icon),
                contentDescription = "google_icon",
                tint = Color.Unspecified
            )
            Spacer(modifier = Modifier.size(20.dp))
        }
    }
}