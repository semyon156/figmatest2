package com.example.myapplication.presentation.viewmodels

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch

class SplashViewModel(): ViewModel() {
    private val _splash: MutableStateFlow<Boolean> = MutableStateFlow(true)
    val splash: StateFlow<Boolean> = _splash.asStateFlow()

    init {
        start()
    }

    private fun updateSplash(newValue: Boolean) {
        _splash.value = newValue
    }

    private fun start() {
        viewModelScope.launch {
            delay(2000)
            updateSplash(false)
        }
    }
}