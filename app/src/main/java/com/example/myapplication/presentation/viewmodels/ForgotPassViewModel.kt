package com.example.myapplication.presentation.viewmodels

import android.content.Context
import android.util.Log
import android.widget.Toast
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import io.github.jan.supabase.SupabaseClient
import io.github.jan.supabase.gotrue.SessionStatus
import io.github.jan.supabase.gotrue.auth
import io.github.jan.supabase.gotrue.providers.builtin.Email
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch

class ForgotPassViewModel(
    private val supabase: SupabaseClient,
    private val context: Context
): ViewModel() {
    private val _textEmail: MutableStateFlow<String> = MutableStateFlow("")
    val textEmail: StateFlow<String> = _textEmail.asStateFlow()

    private val _box1: MutableStateFlow<String> = MutableStateFlow("")
    val box1: StateFlow<String> = _box1.asStateFlow()

    private val _box2: MutableStateFlow<String> = MutableStateFlow("")
    val box2: StateFlow<String> = _box2.asStateFlow()

    private val _box3: MutableStateFlow<String> = MutableStateFlow("")
    val box3: StateFlow<String> = _box3.asStateFlow()

    private val _box4: MutableStateFlow<String> = MutableStateFlow("")
    val box4: StateFlow<String> = _box4.asStateFlow()

    private val _box5: MutableStateFlow<String> = MutableStateFlow("")
    val box5: StateFlow<String> = _box5.asStateFlow()

    private val _box6: MutableStateFlow<String> = MutableStateFlow("")
    val box6: StateFlow<String> = _box6.asStateFlow()

    private val _errorEmail: MutableStateFlow<Boolean> = MutableStateFlow(false)
    val errorEmail: StateFlow<Boolean> = _errorEmail.asStateFlow()

    private val _auth: MutableStateFlow<Boolean> = MutableStateFlow(false)
    val auth: StateFlow<Boolean> = _auth.asStateFlow()

    fun updateTextEmail(newValue: String) {
        _textEmail.value = newValue
    }

    fun updateOTPBox1(newValue: String) {
        _box1.value = newValue
    }

    fun updateOTPBox2(newValue: String) {
        _box2.value = newValue
    }

    fun updateOTPBox3(newValue: String) {
        _box3.value = newValue
    }

    fun updateOTPBox4(newValue: String) {
        _box4.value = newValue
    }

    fun updateOTPBox5(newValue: String) {
        _box5.value = newValue
    }

    fun updateOTPBox6(newValue: String) {
        _box6.value = newValue
    }

    fun checkEmail(): Boolean {
        val result = !textEmail.value.matches(Regex("^[a-z0-9]+@[a-z0-9]+\\.ru$"))
        _errorEmail.value = result
        return result
    }

    fun sendOTP() {
        viewModelScope.launch(Dispatchers.IO) {
            try {
                //supabase.auth.resetPasswordForEmail(email = textEmail.value)
            } catch (e: Exception) {
                viewModelScope.launch(Dispatchers.Main) {
                    Toast.makeText(context, "Неверные данные", Toast.LENGTH_SHORT).show()
                }
            }
        }
    }

    fun setNewPass() {
        viewModelScope.launch(Dispatchers.IO) {
            try {
                supabase.auth.resetPasswordForEmail(
                    email = textEmail.value
                )
            } catch (e: Exception) {
                viewModelScope.launch(Dispatchers.Main) {
                    Toast.makeText(context, "Неверные данные", Toast.LENGTH_SHORT).show()
                }
            }
        }
    }
}

class ForgotPassViewModelFactory(private val supabase: SupabaseClient, private val context: Context): ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return ForgotPassViewModel(
            supabase = supabase,
            context = context
        ) as T
    }
}