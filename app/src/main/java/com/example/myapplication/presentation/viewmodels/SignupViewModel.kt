package com.example.myapplication.presentation.viewmodels

import android.content.Context
import android.widget.Toast
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import io.github.jan.supabase.SupabaseClient
import io.github.jan.supabase.gotrue.auth
import io.github.jan.supabase.gotrue.providers.builtin.Email
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch

class SignupViewModel(
    private val context: Context,
    private val supabase: SupabaseClient
): ViewModel() {
    private val _signUpName: MutableStateFlow<String> = MutableStateFlow("")
    val signUpName: StateFlow<String> = _signUpName.asStateFlow()

    private val _signUpPhone: MutableStateFlow<String> = MutableStateFlow("")
    val signUpPhone: StateFlow<String> = _signUpPhone.asStateFlow()

    private val _signUpEmail: MutableStateFlow<String> = MutableStateFlow("")
    val signUpEmail: StateFlow<String> = _signUpEmail.asStateFlow()

    private val _signUpPass: MutableStateFlow<String> = MutableStateFlow("")
    val signUpPass: StateFlow<String> = _signUpPass.asStateFlow()

    private val _signUpPass2: MutableStateFlow<String> = MutableStateFlow("")
    val signUpPass2: StateFlow<String> = _signUpPass2.asStateFlow()

    private val _checkBox: MutableStateFlow<Boolean> = MutableStateFlow(false)
    val checkBox: StateFlow<Boolean> = _checkBox.asStateFlow()

    private val _errorEmail: MutableStateFlow<Boolean> = MutableStateFlow(false)
    val errorEmail: StateFlow<Boolean> = _errorEmail.asStateFlow()

    private val _errorPass: MutableStateFlow<Boolean> = MutableStateFlow(false)
    val errorPass: StateFlow<Boolean> = _errorPass.asStateFlow()

    fun updateSignUpName(newValue: String) {
        _signUpName.value = newValue
    }

    fun updateSignUpPhone(newValue: String) {
        _signUpPhone.value = newValue
    }

    fun updateSignUpEmail(newValue: String) {
        _signUpEmail.value = newValue
    }

    fun updateSignUpPass(newValue: String) {
        _signUpPass.value = newValue
    }

    fun updateSignUpPass2(newValue: String) {
        _signUpPass2.value = newValue
    }

    fun updateCheckBox(newValue: Boolean) {
        _checkBox.value = newValue
    }

    fun goToast(text: String) {
        Toast.makeText(context, text, Toast.LENGTH_SHORT).show()
    }

    fun checkEmail(): Boolean {
        val result = !signUpEmail.value.matches(Regex("^[a-z0-9]+@[a-z0-9]+\\.ru$"))
        _errorEmail.value = result
        return result
    }

    fun checkPass(): Boolean {
        val result = signUpPass.value != signUpPass2.value
        _errorPass.value = result
        return result
    }

    fun signupUser() {
        viewModelScope.launch(Dispatchers.IO) {
            val auth = supabase.auth
            val user = auth.signUpWith(Email) {
                email = signUpEmail.value
                password = signUpPass.value
            }
        }
    }
}

class SignupViewModelFactory(private val context: Context, private val supabase: SupabaseClient): ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return SignupViewModel(
            context = context,
            supabase = supabase
        ) as T
    }
}