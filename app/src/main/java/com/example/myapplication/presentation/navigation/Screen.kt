package com.example.myapplication.presentation.navigation

sealed class Screen(
    val route: String
) {
    object Splash: Screen("splash")
    object Onboarding: Screen("onBoarding")
    object Holder: Screen("holder")
    object SignUp: Screen("signUp")
    object Login: Screen("logIn")
    object ForgotPass: Screen("forgotPass")
}