package com.example.myapplication.presentation.viewmodels

import android.content.SharedPreferences
import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.myapplication.R
import com.example.myapplication.data.repository.SkipOnboardingImpl
import com.example.myapplication.domain.usecase.GetDataSkipOnboarding
import com.example.myapplication.domain.usecase.WriteDataSkipOnboarding
import com.example.myapplication.presentation.model.QueueOnboarding
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import java.util.LinkedList
import java.util.Queue

class OnboardingViewModel(
    private val getDataSkipOnboarding: GetDataSkipOnboarding,
    private val writeDataSkipOnboarding: WriteDataSkipOnboarding
): ViewModel() {
    private val _queue: MutableStateFlow<Queue<QueueOnboarding>> = MutableStateFlow(LinkedList())
    val queue: StateFlow<Queue<QueueOnboarding>> = _queue.asStateFlow()

    init {
        Log.d("LOG123", "init")
        val skip = getDataSkipOnboarding.execute()
        _queue.value.add(QueueOnboarding("Quick Delivery At Your Doorstep", "Enjoy quick pick-up and delivery to your destination", R.drawable.onboarding_1))
        _queue.value.add(QueueOnboarding("Flexible Payment", "Different modes of payment either before and after delivery without stress", R.drawable.onboarding_2))
        _queue.value.add(QueueOnboarding("Real-time Tracking", "Track your packages/items from the comfort of your home till final destination", R.drawable.onboarding_3))
        when (skip) {
            1 -> _queue.value.remove()
            2 -> {
                _queue.value.remove()
                _queue.value.remove()
            }
            3 -> _queue.value.clear()
        }
    }

    fun clickNext() {
        _queue.value.poll()
        writeDataSkipOnboarding.execute(getDataSkipOnboarding.execute() + 1)
    }

    fun clickSkip() {
        _queue.value.clear()
        writeDataSkipOnboarding.execute(3)
    }
}

class OnboardingViewModelFactory(sharedPreferences: SharedPreferences): ViewModelProvider.Factory {
    private val skipOnboarding by lazy { SkipOnboardingImpl(sharedPreferences) }
    private val getDataSkipOnboarding by lazy { GetDataSkipOnboarding(skipOnboarding) }
    private val writeDataSkipOnboarding by lazy { WriteDataSkipOnboarding(skipOnboarding) }
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return OnboardingViewModel(
            getDataSkipOnboarding = getDataSkipOnboarding,
            writeDataSkipOnboarding = writeDataSkipOnboarding
        ) as T
    }
}