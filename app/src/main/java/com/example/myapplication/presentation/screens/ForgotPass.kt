package com.example.myapplication.presentation.screens

import android.content.Context
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.BasicTextField
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.viewmodel.compose.viewModel
import com.example.myapplication.R
import com.example.myapplication.presentation.navigation.Screen
import com.example.myapplication.presentation.ui.theme.Text_Black_1
import com.example.myapplication.presentation.ui.theme.Text_Gray_1
import com.example.myapplication.presentation.ui.theme.roboto
import com.example.myapplication.presentation.viewmodels.ForgotPassViewModel
import com.example.myapplication.presentation.viewmodels.ForgotPassViewModelFactory
import com.example.myapplication.presentation.viewmodels.LoginViewModel
import com.example.myapplication.presentation.viewmodels.LoginViewModelFactory
import io.github.jan.supabase.SupabaseClient

@Composable
fun OTPBox(isEmpty: Boolean, isError: Boolean, value: String, onChangeValue: (String) -> Unit) {
    Box(
        modifier = Modifier
            .size(32.dp)
            .border(1.dp, color = Text_Gray_1), contentAlignment = Alignment.Center
    ) {
        BasicTextField(
            value = value, onValueChange = { if (it.length <= 1) onChangeValue(it) }, textStyle = TextStyle(
                fontFamily = roboto,
                fontSize = 14.sp,
                fontWeight = FontWeight.Normal,
                color = Color.Black,
                textAlign = TextAlign.Center
            ),
            modifier = Modifier.fillMaxWidth()
        )
    }
}

@Composable
fun ForgotPassScreen(
    supabase: SupabaseClient,
    context: Context,
    vm: ForgotPassViewModel = viewModel(
        factory = ForgotPassViewModelFactory(
            supabase = supabase,
            context = context
        )
    )
) {
    val textEmail = vm.textEmail.collectAsState().value
    val errorEmail = vm.errorEmail.collectAsState().value
    val box1 = vm.box1.collectAsState().value
    val box2 = vm.box2.collectAsState().value
    val box3 = vm.box3.collectAsState().value
    val box4 = vm.box4.collectAsState().value
    val box5 = vm.box5.collectAsState().value
    val box6 = vm.box6.collectAsState().value

    Column(
        modifier = Modifier
            .fillMaxSize()
            .background(MaterialTheme.colorScheme.background)
            .padding(start = 20.dp, top = 100.dp, end = 20.dp)
            .verticalScroll(rememberScrollState())
    ) {
        Text(
            text = "OTP Verification",
            style = TextStyle(
                fontFamily = roboto,
                fontSize = 24.sp,
                fontWeight = FontWeight.Medium,
                color = Text_Black_1
            )
        )
        Spacer(modifier = Modifier.size(6.dp))
        Text(
            text = "Enter the 6 digit numbers sent to your email",
            style = TextStyle(
                fontFamily = roboto,
                fontSize = 14.sp,
                fontWeight = FontWeight.Medium,
                color = Text_Gray_1
            )
        )
        BoxField(
            textValue = textEmail,
            onTextChange = { vm.updateTextEmail(it) },
            hintText = "***********@mail.com",
            onError = errorEmail,
            isPassword = false,
            headText = "Email Address"
        )
        Button(
            onClick = {
                if (textEmail != ""
                ) {
                    vm.checkEmail()
                    if (!vm.checkEmail()) {
                        /*navController.navigate(Screen.Login.route) {
                            popUpTo(0)
                        }*/
                        vm.setNewPass()
                    }
                }
            },
            modifier = Modifier
                .height(46.dp)
                .fillMaxWidth(),
            shape = RoundedCornerShape(4.dp),
            colors = ButtonDefaults.buttonColors(
                containerColor = if (
                    textEmail != ""
                )
                    MaterialTheme.colorScheme.primary else Text_Gray_1
            )
        ) {
            Text(
                text = "Send OTP",
                style = TextStyle(
                    fontFamily = roboto,
                    fontSize = 16.sp,
                    fontWeight = FontWeight.Bold
                )
            )
        }
        Spacer(modifier = Modifier.size(20.dp))
        Row(modifier = Modifier.fillMaxWidth(), horizontalArrangement = Arrangement.SpaceAround) {
            OTPBox(isEmpty = false, isError = false, value = box1, onChangeValue = { vm.updateOTPBox1(it) })
            OTPBox(isEmpty = false, isError = false, value = box2, onChangeValue = { vm.updateOTPBox2(it) })
            OTPBox(isEmpty = false, isError = false, value = box3, onChangeValue = { vm.updateOTPBox3(it) })
            OTPBox(isEmpty = false, isError = false, value = box4, onChangeValue = { vm.updateOTPBox4(it) })
            OTPBox(isEmpty = false, isError = false, value = box5, onChangeValue = { vm.updateOTPBox5(it) })
            OTPBox(isEmpty = false, isError = false, value = box6, onChangeValue = { vm.updateOTPBox6(it) })
        }
        Spacer(modifier = Modifier.size(10.dp))
        Button(
            onClick = {
                if (textEmail != ""
                ) {
                    vm.checkEmail()
                    if (!vm.checkEmail()) {
                        /*navController.navigate(Screen.Login.route) {
                            popUpTo(0)
                        }*/
                        vm.setNewPass()
                    }
                }
            },
            modifier = Modifier
                .height(46.dp)
                .fillMaxWidth(),
            shape = RoundedCornerShape(4.dp),
            colors = ButtonDefaults.buttonColors(
                containerColor = if (
                    textEmail != ""
                )
                    MaterialTheme.colorScheme.primary else Text_Gray_1
            )
        ) {
            Text(
                text = "Set New Password",
                style = TextStyle(
                    fontFamily = roboto,
                    fontSize = 16.sp,
                    fontWeight = FontWeight.Bold
                )
            )
        }
    }
}