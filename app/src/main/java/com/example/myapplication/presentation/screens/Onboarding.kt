package com.example.myapplication.presentation.screens

import android.content.SharedPreferences
import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.animateContentSize
import androidx.compose.animation.expandIn
import androidx.compose.animation.fadeIn
import androidx.compose.animation.fadeOut
import androidx.compose.animation.shrinkOut
import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.DisposableEffect
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.font.toFontFamily
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavController
import androidx.navigation.compose.rememberNavController
import com.example.myapplication.R
import com.example.myapplication.presentation.navigation.Screen
import com.example.myapplication.presentation.ui.theme.MyApplicationTheme
import com.example.myapplication.presentation.ui.theme.roboto
import com.example.myapplication.presentation.viewmodels.OnboardingViewModel
import com.example.myapplication.presentation.viewmodels.OnboardingViewModelFactory

@Composable
fun OnboardingScreen(
    sharedPreferences: SharedPreferences,
    vm: OnboardingViewModel = viewModel(
        factory = OnboardingViewModelFactory(sharedPreferences)
    ),
    navController: NavController
) {
    val queue = vm.queue.collectAsState().value
    var clickNext by rememberSaveable { mutableStateOf(false) }
    var clickSkip by rememberSaveable { mutableStateOf(false) }


    LaunchedEffect(queue.size) {
        if (queue.size == 0) {
            navController.navigate(Screen.SignUp.route) { popUpTo(0) }
        }
    }

    LaunchedEffect(clickNext) {
        if (clickNext) {
            vm.clickNext()
            clickNext = false
        }
    }

    LaunchedEffect(clickSkip) {
        if (clickSkip) {
            vm.clickSkip()
            clickSkip = false
        }
    }

    Column(
        modifier = Modifier
            .fillMaxSize()
            .background(MaterialTheme.colorScheme.background)
            .padding(horizontal = 20.dp),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Spacer(modifier = Modifier.size(if (queue.size == 3) 60.dp else if (queue.size == 2) 80.dp else 100.dp))
    }
    Box(
        contentAlignment = Alignment.Center, modifier = Modifier
            .fillMaxWidth()
            .padding(top = 40.dp)
    ) {
        Icon(
            painterResource(if (queue.size > 0) queue.element()!!.image else R.drawable.logo_splash_dark),
            contentDescription = "Onboarding image",
            tint = Color.Unspecified
        )
    }

    Column(
        modifier = Modifier
            .fillMaxWidth()
            .padding(top = 440.dp, start = 20.dp, end = 20.dp)
    ) {
        Column(
            modifier = Modifier
                .fillMaxWidth()
                .padding(horizontal = 40.dp)
        ) {
            Text(
                text = if (queue.size > 0) queue.element()!!.headText else "",
                color = MaterialTheme.colorScheme.primary,
                modifier = Modifier.fillMaxWidth(),
                textAlign = TextAlign.Center,
                style = TextStyle(
                    fontFamily = roboto,
                    fontSize = 24.sp,
                    fontWeight = FontWeight.Bold
                )
            )
            Spacer(modifier = Modifier.size(if (queue.size != 1) 6.dp else 12.dp))
            Text(
                text = if (queue.size > 0) queue.element()!!.bodyText else "",
                color = MaterialTheme.colorScheme.onBackground,
                modifier = Modifier.fillMaxWidth(),
                textAlign = TextAlign.Center,
                style = TextStyle(
                    fontFamily = roboto,
                    fontSize = 16.sp,
                    fontWeight = FontWeight.Normal
                )
            )
        }
        Spacer(modifier = Modifier.size(80.dp))
    }
    if (queue.size > 1) {
        Row(
            modifier = Modifier
                .fillMaxWidth()
                .padding(top = 620.dp, start = 20.dp, end = 20.dp),
            horizontalArrangement = Arrangement.SpaceBetween
        ) {
            Button(
                onClick = { clickSkip = true },
                modifier = Modifier.size(width = 100.dp, height = 50.dp),
                colors = ButtonDefaults.buttonColors(containerColor = MaterialTheme.colorScheme.background),
                shape = RoundedCornerShape(4.69.dp),
                border = BorderStroke(width = 1.dp, color = MaterialTheme.colorScheme.primary)
            ) {
                Text(
                    text = "Skip",
                    style = TextStyle(
                        fontFamily = roboto,
                        fontSize = 14.sp,
                        fontWeight = FontWeight.Bold,
                        color = MaterialTheme.colorScheme.primary
                    )
                )
            }
            Button(
                onClick = { clickNext = true },
                modifier = Modifier.size(width = 100.dp, height = 50.dp),
                shape = RoundedCornerShape(4.69.dp)
            ) {
                Text(
                    text = "Next",
                    style = TextStyle(
                        fontFamily = roboto,
                        fontSize = 14.sp,
                        fontWeight = FontWeight.Bold
                    )
                )
            }
        }
    } else {
        Column(
            modifier = Modifier
                .fillMaxWidth()
                .padding(top = 620.dp, start = 20.dp, end = 20.dp)
        ) {
            Button(
                onClick = { navController.navigate(Screen.SignUp.route) },
                modifier = Modifier
                    .height(46.dp)
                    .fillMaxWidth(),
                shape = RoundedCornerShape(4.dp)
            ) {
                Text(
                    text = "Sign Up",
                    style = TextStyle(
                        fontFamily = roboto,
                        fontSize = 16.sp,
                        fontWeight = FontWeight.Bold
                    )
                )
            }
            Spacer(modifier = Modifier.size(20.dp))
            Row(modifier = Modifier.fillMaxWidth(), horizontalArrangement = Arrangement.Center) {
                Text(
                    text = "Already have an account?",
                    style = TextStyle(
                        fontFamily = roboto,
                        fontSize = 14.sp,
                        fontWeight = FontWeight.Normal,
                        color = MaterialTheme.colorScheme.onSecondary
                    )
                )
                Text(text = "Sign in",
                    style = TextStyle(
                        fontFamily = roboto,
                        fontSize = 14.sp,
                        fontWeight = FontWeight.Medium,
                        color = MaterialTheme.colorScheme.primary
                    ),
                    modifier = Modifier.clickable { clickNext = true }
                )
            }
        }
    }
}