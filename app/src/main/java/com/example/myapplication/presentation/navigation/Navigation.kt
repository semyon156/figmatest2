package com.example.myapplication.presentation.navigation

import android.content.Context
import android.content.SharedPreferences
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import com.example.myapplication.presentation.screens.ForgotPassScreen
import com.example.myapplication.presentation.screens.LoginScreen
import com.example.myapplication.presentation.screens.OnboardingScreen
import com.example.myapplication.presentation.screens.SignUpScreen
import com.example.myapplication.presentation.screens.SplashScreen
import io.github.jan.supabase.SupabaseClient

@Composable
fun NavigationScreen(sharedPreferences: SharedPreferences, context: Context, supabase: SupabaseClient) {
    val navController = rememberNavController()
    
    NavHost(navController = navController, startDestination = Screen.Splash.route) {
        composable(route = Screen.Splash.route) {
            SplashScreen(navController = navController)
        }
        composable(route = Screen.Onboarding.route) {
            OnboardingScreen(navController = navController, sharedPreferences = sharedPreferences)
        }
        composable(route = Screen.Holder.route) {
            Box(modifier = Modifier.fillMaxSize()) {

            }
        }
        composable(route = Screen.SignUp.route) {
            SignUpScreen(context = context, navController = navController, supabase = supabase)
        }
        composable(route = Screen.Login.route) {
            LoginScreen(supabase = supabase, context = context, navController = navController)
        }
        composable(route = Screen.ForgotPass.route) {
            ForgotPassScreen(supabase = supabase, context = context)
        }
    }
}