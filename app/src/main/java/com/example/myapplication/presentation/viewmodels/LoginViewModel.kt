package com.example.myapplication.presentation.viewmodels

import android.content.Context
import android.util.Log
import android.widget.Toast
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import io.github.jan.supabase.SupabaseClient
import io.github.jan.supabase.gotrue.SessionStatus
import io.github.jan.supabase.gotrue.auth
import io.github.jan.supabase.gotrue.providers.builtin.Email
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch

class LoginViewModel(
    private val supabase: SupabaseClient,
    private val context: Context
): ViewModel() {
    private val _textEmail: MutableStateFlow<String> = MutableStateFlow("")
    val textEmail: StateFlow<String> = _textEmail.asStateFlow()

    private val _textPass: MutableStateFlow<String> = MutableStateFlow("")
    val textPass: StateFlow<String> = _textPass.asStateFlow()

    private val _errorEmail: MutableStateFlow<Boolean> = MutableStateFlow(false)
    val errorEmail: StateFlow<Boolean> = _errorEmail.asStateFlow()

    private val _auth: MutableStateFlow<Boolean> = MutableStateFlow(false)
    val auth: StateFlow<Boolean> = _auth.asStateFlow()

    fun updateTextEmail(newValue: String) {
        _textEmail.value = newValue
    }

    fun updateTextPass(newValue: String) {
        _textPass.value = newValue
    }

    fun checkEmail(): Boolean {
        val result = !textEmail.value.matches(Regex("^[a-z0-9]+@[a-z0-9]+\\.ru$"))
        _errorEmail.value = result
        return result
    }

    init {
        viewModelScope.launch(Dispatchers.IO) {
            supabase.auth.sessionStatus.collect {
                when(it) {
                    is SessionStatus.Authenticated -> Log.d("auth123", "auth")
                    SessionStatus.LoadingFromStorage -> Log.d("auth123", "Loading from storage")
                    SessionStatus.NetworkError -> Log.d("auth123", "network error")
                    SessionStatus.NotAuthenticated -> Log.d("auth123", "none auth")
                }
            }
        }
    }

    fun loginUser() {
        viewModelScope.launch(Dispatchers.IO) {
            try {
                supabase.auth.signInWith(Email) {
                    email = textEmail.value
                    password = textPass.value
                }
            } catch (e: Exception) {
                viewModelScope.launch(Dispatchers.Main) {
                    Toast.makeText(context, "Неверные данные", Toast.LENGTH_SHORT).show()
                }
            }
        }
    }
}

class LoginViewModelFactory(private val supabase: SupabaseClient, private val context: Context): ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return LoginViewModel(
            supabase = supabase,
            context = context
        ) as T
    }
}