package com.example.myapplication.presentation.screens

import android.content.Context
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavController
import com.example.myapplication.R
import com.example.myapplication.presentation.navigation.Screen
import com.example.myapplication.presentation.ui.theme.Text_Black_1
import com.example.myapplication.presentation.ui.theme.Text_Gray_1
import com.example.myapplication.presentation.ui.theme.roboto
import com.example.myapplication.presentation.viewmodels.LoginViewModel
import com.example.myapplication.presentation.viewmodels.LoginViewModelFactory
import io.github.jan.supabase.SupabaseClient

@Composable
fun LoginScreen(
    supabase: SupabaseClient,
    navController: NavController,
    context: Context,
    vm: LoginViewModel = viewModel(factory = LoginViewModelFactory(supabase = supabase, context = context))
) {
    val textEmail = vm.textEmail.collectAsState().value
    val textPass = vm.textPass.collectAsState().value
    val errorEmail = vm.errorEmail.collectAsState().value

    Column(
        modifier = Modifier
            .fillMaxSize()
            .background(MaterialTheme.colorScheme.background)
            .padding(start = 20.dp, top = 100.dp, end = 20.dp)
            .verticalScroll(rememberScrollState())
    ) {
        Text(
            text = "Welcome Back",
            style = TextStyle(
                fontFamily = roboto,
                fontSize = 24.sp,
                fontWeight = FontWeight.Medium,
                color = Text_Black_1
            )
        )
        Spacer(modifier = Modifier.size(6.dp))
        Text(
            text = "Fill in your email and password to continue",
            style = TextStyle(
                fontFamily = roboto,
                fontSize = 14.sp,
                fontWeight = FontWeight.Medium,
                color = Text_Gray_1
            )
        )
        Spacer(modifier = Modifier.size(20.dp))
        BoxField(
            textValue = textEmail,
            onTextChange = { vm.updateTextEmail(it) },
            hintText = "***********@mail.com",
            onError = errorEmail,
            isPassword = false,
            headText = "Email Address"
        )
        Spacer(modifier = Modifier.size(20.dp))
        BoxField(
            textValue = textPass,
            onTextChange = { vm.updateTextPass(it) },
            hintText = "**********",
            onError = false,
            isPassword = true,
            headText = "Password"
        )
        Spacer(modifier = Modifier.size(10.dp))
        Row(
            modifier = Modifier.fillMaxWidth(),
            horizontalArrangement = Arrangement.SpaceBetween
        ) {
            Row {
                Icon(painterResource(id = R.drawable.check_box_remember_pass), contentDescription = "check_box_remember_pass")
                Text(
                    text = "Remember password",
                    style = TextStyle(
                        fontFamily = roboto,
                        fontSize = 12.sp,
                        fontWeight = FontWeight.Medium,
                        color = Text_Gray_1
                    )
                )
            }
            Text(
                text = "Remember password",
                style = TextStyle(
                    fontFamily = roboto,
                    fontSize = 12.sp,
                    fontWeight = FontWeight.Medium,
                    color = MaterialTheme.colorScheme.primary
                ),
                modifier = Modifier.clickable {
                    navController.navigate(Screen.ForgotPass.route) {
                        popUpTo(0)
                    }
                }
            )
        }
        Spacer(modifier = Modifier.size(160.dp))
        Button(
            onClick = {
                if (textEmail != "" &&
                    textPass != ""
                ) {
                    vm.checkEmail()
                    if (!vm.checkEmail()) {
                        /*navController.navigate(Screen.Login.route) {
                            popUpTo(0)
                        }*/
                        vm.loginUser()
                    }
                }
            },
            modifier = Modifier
                .height(46.dp)
                .fillMaxWidth(),
            shape = RoundedCornerShape(4.dp),
            colors = ButtonDefaults.buttonColors(
                containerColor = if (
                    textEmail != "" &&
                    textPass != ""
                )
                    MaterialTheme.colorScheme.primary else Text_Gray_1
            )
        ) {
            Text(
                text = "Log in",
                style = TextStyle(
                    fontFamily = roboto,
                    fontSize = 16.sp,
                    fontWeight = FontWeight.Bold
                )
            )
        }
        Column(
            horizontalAlignment = Alignment.CenterHorizontally,
            modifier = Modifier.fillMaxWidth()
        ) {
            Row {
                Text(
                    text = "Already have an account?",
                    fontFamily = roboto,
                    fontSize = 14.sp,
                    fontWeight = FontWeight.Normal,
                    color = Text_Gray_1,
                )
                Text(
                    text = "Sign Up",
                    fontFamily = roboto,
                    fontSize = 14.sp,
                    fontWeight = FontWeight.Medium,
                    color = MaterialTheme.colorScheme.primary,
                    modifier = Modifier.clickable {

                    }
                )
            }
            Spacer(modifier = Modifier.size(20.dp))
            Text(
                text = "or log in using",
                fontFamily = roboto,
                fontSize = 14.sp,
                fontWeight = FontWeight.Normal,
                color = Text_Gray_1
            )
            Spacer(modifier = Modifier.size(6.dp))
            Icon(
                painterResource(id = R.drawable.google_icon),
                contentDescription = "google_icon",
                tint = Color.Unspecified
            )
            Spacer(modifier = Modifier.size(20.dp))
        }
    }
}