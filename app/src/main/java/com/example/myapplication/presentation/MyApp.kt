package com.example.myapplication.presentation

import android.app.Application
import android.content.SharedPreferences
import com.example.myapplication.data.storage.sharedprefs.SharedPreferencesStorage
import io.github.jan.supabase.SupabaseClient
import io.github.jan.supabase.createSupabaseClient
import io.github.jan.supabase.gotrue.Auth
import io.github.jan.supabase.gotrue.auth
import io.github.jan.supabase.postgrest.Postgrest

class MyApp() : Application() {
    companion object {
        lateinit var sharedPreferences: SharedPreferences
        lateinit var supabase: SupabaseClient
        lateinit var auth: Auth
    }

    override fun onCreate() {
        super.onCreate()
        sharedPreferences = SharedPreferencesStorage.getSharedPrefs(applicationContext)
        supabase = createSupabaseClient(
            supabaseUrl = "https://xxfybzlohqrjvckttvis.supabase.co",
            supabaseKey = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJzdXBhYmFzZSIsInJlZiI6Inh4ZnliemxvaHFyanZja3R0dmlzIiwicm9sZSI6InNlcnZpY2Vfcm9sZSIsImlhdCI6MTcwNjUwNzkyMCwiZXhwIjoyMDIyMDgzOTIwfQ.4b0AHTLuTjsMZJ0sfuGBoYIcFj4Y_25q20sKggsZYno"
        ) {
            install(Auth)
            install(Postgrest)
        }
    }
}