package com.example.myapplication.presentation.model

data class QueueOnboarding(
    val headText: String,
    val bodyText: String,
    val image: Int
)
