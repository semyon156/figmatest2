package com.example.myapplication.presentation

import android.content.SharedPreferences
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import com.example.myapplication.data.storage.sharedprefs.SharedPreferencesStorage
import com.example.myapplication.presentation.navigation.NavigationScreen
import com.example.myapplication.presentation.ui.theme.MyApplicationTheme
import io.github.jan.supabase.SupabaseClient

class MainActivity : ComponentActivity() {
    private lateinit var sharedPref: SharedPreferences
    private lateinit var subabase: SupabaseClient

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        sharedPref = MyApp.sharedPreferences
        subabase = MyApp.supabase

        setContent {
            MyApplicationTheme {
                NavigationScreen( sharedPreferences = sharedPref, context = applicationContext, supabase = subabase)
            }
        }
    }
}