package com.example.myapplication.presentation.ui.theme

import androidx.compose.ui.graphics.Color

val Blue = Color(0xFF0560FA)

val Text_Black_1 = Color(0xFF3A3A3A)
val Text_Gray_1 = Color(0xFFA7A7A7)
val Text_Gray_2 = Color(0xFFCFCFCF)
val Text_Yellow_1 = Color(0xFFEBBC2E)
val MyError = Color(0xFFED3A3A)
val Background_dark = Color(0xFF000D1D)
val Background = Color(0xFFFFFFFF)