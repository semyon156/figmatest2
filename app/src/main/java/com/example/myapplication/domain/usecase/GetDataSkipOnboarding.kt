package com.example.myapplication.domain.usecase

import com.example.myapplication.domain.repository.SkipOnboarding

class GetDataSkipOnboarding(private val skipOnboarding: SkipOnboarding) {
    fun execute(): Int {
        return skipOnboarding.getData()
    }
}