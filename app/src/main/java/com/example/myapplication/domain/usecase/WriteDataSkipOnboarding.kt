package com.example.myapplication.domain.usecase

import com.example.myapplication.domain.repository.SkipOnboarding

class WriteDataSkipOnboarding(private val skipOnboarding: SkipOnboarding) {
    fun execute(data: Int) {
        skipOnboarding.writeData(data)
    }
}