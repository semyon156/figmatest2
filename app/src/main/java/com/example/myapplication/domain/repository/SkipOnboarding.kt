package com.example.myapplication.domain.repository

interface SkipOnboarding {
    fun getData(): Int
    fun writeData(data: Int)
}